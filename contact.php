<?php include 'inc/header.php';?>
<link rel="stylesheet" href="css/contactUs.css">

<?php 
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
        $sendMsg = $mess->sendMessage($_POST);
    }
?> 


<div class="container_about">
		<div class="left-amitosh">


			<h2>Contact Us!</h2>
			<?php 
			    if (isset($sendMsg)) {
			        echo $sendMsg;
			    }
			?>
			<form action="#" method="post">
				<div class="agile1">
					<h3>Name</h3>
					<input type="text" name="name" class="name" placeholder="Enter Your Name" required="">
				</div>
				<div class="agile1">
					<h3>Company</h3>
					<input type="text" name="company" class="company" placeholder="Enter Your company" required="">
				</div>
				<div class="agile1">
					<h3>Telephone</h3>
					<input type="text" name="phone" class="phone" placeholder="Ener Your Phone Number" required="">
				</div>
				<div class="agile1">
					<h3>Email</h3>
					<input type="text" name="email" class="email" placeholder="Enter Valid Email." required="">
				</div>
				<div class="agile1">
					<h3>Message</h3>
					<textarea  name="message" placeholder="Enter Your Message"  required=""></textarea>
				</div>	
				<input type="submit" name="submit" value="Send Message">
			</form>
		</div>
		<div class="right-amitosh">
			
			<h3 class="amitosh">Our Location</h3>
			<div class="agile1">
				<h3>Reach Us</h3>
				<div id="map" class="map">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d912.9242500155113!2d90.38882800304114!3d23.758182107394223!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8a6b5c8b851%3A0xbd30fd584b97cb22!2sTejgaon+College!5e0!3m2!1sen!2sbd!4v1513193560454" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>	
			</div>
			<h3 class="amitosh">Stay In Touch</h3>
			<div class="agile1">
				<h3>Social Icons</h3>
				<div class="socialicons w3">
					<ul>
                                            <li><a class="facebook" target="_blank" href="https://www.facebook.com/sanzid.samun"></a></li>
                                            <li><a class="twitter"  target="_blank"  href="https://twitter.com/sanzidsamun2"></a></li>
                                            <li><a class="google"  target="_blank"  href="https://plus.google.com/sanzidsamun"></a></li>
                                            <li><a class="linkedin"  target="_blank"  href="https://www.linkedin.com/in/sanzid-samun-890320a3/"></a></li>
                                            <li><a class="pinterest"  target="_blank"  href="#"></a></li>
					</ul>
				</div>
			</div>
		</div>
</div>
<div class="freeSpace">
	
</div>


<?php include 'inc/footer.php';?>
	




